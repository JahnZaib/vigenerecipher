package krypto.cipher;

import javax.crypto.Cipher;

/**
 * Hello world!
 *
 */
public class App 
{
	
	public static String xAppender(String plaintext,String keyword)
	{
		StringBuilder plaintextWithX = new StringBuilder();
		plaintextWithX.append(plaintext); //add plaintext to builder
		int appendXSize = 0;
		if(plaintext.length() % keyword.length()!=0)
		{
		   appendXSize = keyword.length() - plaintext.length() % keyword.length(); //if no rest = 0, else keyword - rest
		}	
		System.out.println(appendXSize);
		for(int i =0; i<appendXSize; i++)
		{
			plaintextWithX.append("x");
		}
		return plaintextWithX.toString();
	}
	
	public static String DeEncrypt(String plaintext, String keyword)
	{  
		
		plaintext = xAppender(plaintext, keyword);
		char plainCiphertext[] = new char [plaintext.length()];
		int keyShift[] = new int[keyword.length()];
		for(int i =0; i<keyword.length(); i++)
		{
			keyShift[i] = Character.getNumericValue(keyword.charAt(i)-i)-1;	
		}
		for(int i = 0,j = 0; i < plaintext.length(); i++,j++)
		{
		  if(j>=keyword.length())
		  {
			  j=0;
		  }
		  int key = keyShift[j];
		  int newIndex = key+i;
		  plainCiphertext[i] = plaintext.charAt(newIndex);
		}
		return String.valueOf(plainCiphertext);
	}
	
	
	public static String computeDecryptionKey( String keyword)
	{
		char encryptionKey[] = new char[keyword.length()];
		for(int i =0; i<keyword.length(); i++)
		{
			encryptionKey[Character.getNumericValue(keyword.charAt(i))-1]=Integer.toString(i+1).charAt(0);
		}
		return String.valueOf(encryptionKey);
	}
	
	
    public static void main( String[] args )
    {
      String plaintext="hallotest";
      String encryption = "241536";
      
      System.out.println(plaintext);
      System.out.println(encryption);
      
      String ciphertext =  DeEncrypt(plaintext, encryption);
      String decryption = computeDecryptionKey(encryption);

      System.out.println(ciphertext);
      System.out.println(decryption);
        
      System.out.println(DeEncrypt(ciphertext, decryption));

    
    }
}
